# Contents of the repository

This project contains the specification and common XSD schemas for the Software Engineering project conducted by the Faculty of Mathematics and Information Sciences of Warsaw University of Technology.

## Technical specification

Technical details in PDF are available in downloads section:
https://bitbucket.org/okulwut/mini-softwareengineering-theprojectgame/downloads/TechnicalDetails-1.3.0.pdf

Comments in XML's and XML Schemas and [resolved issues](https://bitbucket.org/okulwut/mini-softwareengineering-theprojectgame/issues?status=resolved&sort=updated_on) also form a part of the specification

# Language

The working language of the repository is English. Please use it in all the issues and commit comments.